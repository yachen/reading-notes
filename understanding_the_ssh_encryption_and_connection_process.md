#Understanding the SSH Encryption and Connection Process

SSH = secure shell

##Symmetrical Encryption

Symmetrical encryption is a type of encryption where one key can be used to encrypt messages to the opposite party, and also to decrypt the messages received from the other participant. This means that anyone who holds the key can encrypt and decrypt messages to anyone else holding the key.

Symmetric keys are used by SSH in order to encrypt the entire connection. The client and server both contribute toward establishing this key. The key created by this procedure (key exchange algorithm) is session-based and constitutes the actual encryption for the data sent between server and client. Once this is established, the rest of the data must be encrypted with this shared secret. This is done prior to authenticating a client.

SSH can be configured to utilize a variety of different symmetrical cipher systems, including AES, Blowfish, 3DES, CAST128, and Arcfour.

##Asymmetrical Encryption
Asymmetrical encryption is different from symmetrical encryption in that to send data in a single direction, two associated keys are needed - the private key and the public key.

The public key can be freely shared. The mathematical relationship between the public key and the private key allows the public key to encrypt messages that can only be decrypted by the private key (one-way ability).

The private key should be kept entirely secret. It is the only component capable of decrypting messages that were encrypted using the associated public key.

SSH utilizes asymmetric encryption in a few different places. During the initial key exchange process used to set up the symmetrical encryption (used to encrypt the session), asymmetrical encryption is used. In this stage, both parties produce temporary key pairs and exchange the public key in order to produce the shared secret that will be used for symmetrical encryption.